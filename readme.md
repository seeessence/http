# app [![Build Status](https://travis-ci.org/seeessence/server.svg)](https://travis-ci.org/seeessence/app)

```javascript
var app = require('app');
var view = require('view');

app.pipe(
    view.jade('example.jade')
);

app.listen(8080);
```