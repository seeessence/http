var stream = require('stream');
var http = require('http');

var response;

var source = new stream.Transform({
    objectMode: true,
    highWaterMark: 1,
    transform: function (request, encoding, callback) {
        response = encoding;
        callback(null, request);
    }
});

source.on('pipe', function(asd) {
    asd.unpipe(this);
    asd.pipe(destination);
});

var destination = new stream.Transform({
    objectMode: true,
    highWaterMark: 1,
    transform: function (data, encoding, callback) {
        response.end(data, callback);
    }
});

module.exports.pipe = function() {
    for (var name in arguments)
        source.pipe(arguments[name]);
};

module.exports.listen = function (options) {
    http.createServer(
        source.write.bind(source)
    ).listen(options);
};
